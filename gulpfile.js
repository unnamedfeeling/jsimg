var gulp = require('gulp'),
	less = require('gulp-less'),
	errorNotifier = require('gulp-error-notifier'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	minify = require('gulp-minify-css'),
	uglify = require('gulp-uglify-es').default;

var jsList = [
		"./assets/js/index.js",
	];

gulp.task('less', function() {
	gulp.src('./assets/less/style.less')
	.pipe(errorNotifier())
	.pipe(less())
	.pipe(concat('app.min.css'))
	.pipe(minify())
	.pipe(gulp.dest('./htdocs/dist'))
});

gulp.task('scripts', function() {
	gulp.src(jsList)
		.pipe(errorNotifier())
		.pipe(concat('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./htdocs/dist'));
});

gulp.task('watch', function() {
	gulp.watch('./assets/less/*.less', ['less']);
	gulp.watch('./assets/js/*', ['scripts']);
});

gulp.task('default', ['less', 'scripts', 'watch']);
