var app={
	// vars
	vars: {
		input: document.querySelector('.js-generator input'),
		number: 0,
		allImageParams:[]
	},

	// utilities
	init: function(){
		app.setValue();
		app.generateMarkup();
	},
	loadImages: function(params){
		app.clearData();
		return new Promise(function(resolve, reject){
			var urls=app.generateImageUrls(),
				promises=[];

			urls.forEach(function(elem, index, arr){
				promises.push(
					app.generateBlob(elem).then(
						function(result){
							app.generateImgHtml(result, index, elem);
						}
					).catch(function(error){console.log('There was an error: '+error)})
				);
			});

			Promise.all(promises).then(function(){
				resolve();
			})
		});
	},
	generateReport: function(){
		var table=app.generateReportHtml(app.vars.allImageParams);
		app.vars.resultTarget.appendChild(table);
	},

	// helpers
	generateImageUrls: function(params){
		var number=app.vars.number,
			result=[];

		for (var i = 0; i < number; i++) {
			var size=app.generateRandomSize({min:240, max:550}),
				imgUrl=app.generateImgUrl(size);
			result.push(imgUrl);
		}

		return result;
	},
	generateRandomSize: function(params){
		var arr=[params.min, params.max],
			result=[];
		for (var i = 0; i < 2; i++) {
			result.push(Math.floor(Math.random() * (params.max - params.min)) + params.min);
		}
		return result;
	},
	generateImgUrl: function(params){
		var url='https://picsum.photos';
		params.forEach(function(value, key, number){
			url+='/'+value;
		})
		return url;
	},
	generateImgHtml: function(data, index, url){
		var img=document.createElement('img'),
			col = document.createElement('div'),
			wrapper = document.createElement('div'),
			imageInfo = document.createElement('div');

		img.classList.add('img-responsive');
		img.src=URL.createObjectURL(data);
		img.dataset.index=index;

		wrapper.classList.add('imgwrapper');

		wrapper.appendChild(img);

		imageInfo.classList.add('imageinfo');


		img.onload=function(){
			var imgData={
				url: url,
				size: data.size,
				width: this.naturalWidth,
				height: this.naturalHeight
			};

			app.vars.allImageParams.push(imgData);

			imageInfo.insertAdjacentHTML('beforeend', '<p>Размер: '+Math.round(imgData.size/1024*100)/100+' KB</p><p>Ширина: '+imgData.width+'</p><p>Высота: '+imgData.height+'</p>');
			wrapper.appendChild(imageInfo);

			col.appendChild(wrapper);

			col.classList.add('col-sm-6','col-md-3');
			app.vars.imagesTarget.appendChild(col);
		};
	},
	generateBlob: function(url){
		return fetch(url).then(function (response) {return response.blob();});
	},
	setValue: function(){
		app.vars.number=parseInt(app.vars.input.value);
	},
	generateMarkup: function(){
		var generatorParent=document.querySelector('.main-container'),
			imagesContainer='<div class="row js-imagesTarget"></div>',
			resultContainer='<div class="row js-resultTarget"></div>';
		generatorParent.insertAdjacentHTML('beforeend', imagesContainer+resultContainer);
		app.vars.imagesTarget=document.querySelector('.js-imagesTarget');
		app.vars.resultTarget=document.querySelector('.js-resultTarget');
	},
	clearData: function(){
		var imgTarget=app.vars.imagesTarget,
			resultTarget=app.vars.resultTarget;
		app.vars.allImageParams=[];

		while(imgTarget.hasChildNodes()){
			imgTarget.removeChild(imgTarget.firstChild);
		}
		while(resultTarget.hasChildNodes()){
			resultTarget.removeChild(resultTarget.firstChild);
		}
	},
	formatString: function(format){
		var args = Array.prototype.slice.call(arguments, 1);
		return format.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	},
	generateReportHtml: function(params){
		var table=document.createElement('table'),
			row='<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>',
			heading=app.formatString(
				'<thead>'+row+'</thead>',
				'Номер',
				'Ссылка',
				'Размер',
				'Ширина, пикс',
				'Высота, пикс',
			);
		table.insertAdjacentHTML('afterbegin', heading);
		table.insertAdjacentHTML('beforeend', '<tbody></tbody>');
		table.classList.add('table');

		params.forEach(function(elem, index, arr){
			var num=index+1,
				string=app.formatString(row, num, elem.url, Math.round(elem.size/1024*100)/100, elem.width, elem.height);
			table.querySelector('tbody').insertAdjacentHTML('beforeend', string)
		});

		return table;
	}
};


window.addEventListener('load', function(){
	app.init();
});
document.addEventListener('click', function(event){
	switch (true) {
		case event.target.classList.contains('js-loadImages'):
			app.loadImages().then(function(){
				var reportBtn=document.querySelector('.js-generateReport');
				if (reportBtn.classList.contains('hide')) {
					reportBtn.classList.remove('hide');
				}
			});
			break;
		case event.target.classList.contains('js-generateReport'):
			app.generateReport();
			break;
	}
});

app.vars.input.addEventListener('change', function(event){
	app.setValue();
})
